public class Cat{
	public String name;
	public String colour;
	public int age;

	public void SayHi(){
		for(int i=0; i<this.name.length();i++){
			System.out.println("Hi ");
		}
	}
	
	public void Active(){
		if(this.age<=0){
			System.out.println("Your cat is dead ;(");
		}else if(this.age<=365){
			System.out.println("You have a kitten, protect your couches!");
		}else{
			System.out.print("You have an older cat give them treats!");
		}	
	}
	public String getName() {
		return this.name; 
	}
	public String getColour() {
		return this.colour; 
	}
	public int getAge() {
		return this.age; 
	}
	public void setName(String newName) {
		this.name = newName;
	}
	
	public Cat(String name, String colour, int age){
		this.name = name;
		this.colour = colour;
		this.age = age;
	}
}